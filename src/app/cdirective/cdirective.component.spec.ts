import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CdirectiveComponent } from './cdirective.component';

describe('CdirectiveComponent', () => {
  let component: CdirectiveComponent;
  let fixture: ComponentFixture<CdirectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CdirectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CdirectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
