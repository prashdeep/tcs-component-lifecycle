import { Component, OnInit, OnChanges, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, DoCheck, Input } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit,
                                      OnChanges,
                                      DoCheck,
                                      AfterContentInit,
                                      AfterContentChecked,
                                      AfterViewInit,
                                      AfterViewChecked,
                                      OnDestroy {

   @Input('backgroundColor')
   backgroundColor:string;                                         

  constructor() { }

  ngOnInit() {
    console.log('came inside the onInit method');    
  }

  ngOnChanges(){
    console.log('came inside the ng Onchanges method');
  }

  ngDoCheck(){
    console.log('came inside the doCheck method');
  }

  ngAfterContentInit(){
    console.log('came inside the after content init');
  }

  ngAfterContentChecked(){
    console.log('came inside the ng after content checked method');
  }

  ngAfterViewInit(){
    console.log('came inside the after view init method');
  }

  ngAfterViewChecked(){
    console.log('came inside the after view checked method');
  }

  ngOnDestroy(){
    console.log('came inside the on destroy method');
  }
}
