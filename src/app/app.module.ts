import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { StudentComponent } from './student/student.component';
import { StudentService } from './student.service';
import { CdirectiveComponent } from './cdirective/cdirective.component';
import { ChildComponent } from './child/child.component';
import { LifecyclehookDirective } from './lifecyclehook.directive';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    StudentComponent,
    CdirectiveComponent,
    ChildComponent,
    LifecyclehookDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule    
  ],
  providers: [StudentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
