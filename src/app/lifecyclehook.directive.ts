import { Directive, OnInit, OnChanges, DoCheck, OnDestroy, Input, HostBinding, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[lifecycle-directive]'
})
export class LifecyclehookDirective
      implements OnInit, OnChanges, DoCheck, OnDestroy {

  @Input('lifecycle-directive')
  backgroundColor:string;

  @HostBinding("style.backgroundColor")
  backgroundColorStyle:string;

  constructor() { }

  ngOnInit(){
    console.log('came inside the ngOnInit method');
  }

  ngOnChanges(changes:SimpleChanges){
    this.backgroundColorStyle=this.backgroundColor;
    console.log('came inside the on changes method');
  }

  ngDoCheck(){
    console.log('came inside the do check method');
  }

  ngOnDestroy(){
    console.log('came inside the on destroy method');
  }

}
