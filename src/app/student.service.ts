import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable  } from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import { User } from './User';


@Injectable()
export class StudentService {

  constructor(private http:HttpClient) { }

  getAllUsers():Observable<HttpResponse<User>>{
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users', 
      {observe:'response'});
  }

}
