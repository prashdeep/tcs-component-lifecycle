import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-cdirective',
  templateUrl: './cdirective.component.html',
  styleUrls: ['./cdirective.component.css']
})
export class CdirectiveComponent implements OnInit, OnChanges {

  bgColor:string;
  doCheckStr:string;
  toggleChild:boolean;

  constructor() { }

  ngOnInit() {
    console.log('came inside the top component init method')
  }

  ngOnChanges(){
    console.log('came inside the on changes method on the top level component.')
  }

}
